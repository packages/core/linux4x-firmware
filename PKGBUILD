# Maintainer: Mark Wagie <mark at manjaro dot org>

# Arch credits:
# Maintainer: Thomas Bächler <thomas@archlinux.org>

pkgbase=linux4x-firmware
_pkgbase=linux-firmware
pkgname=(linux4x-firmware-whence linux4x-firmware linux4x-amd-ucode
         linux4x-firmware-{nfp,mellanox,marvell,qcom,liquidio,qlogic,bnx2x}
)
#_tag=20211216
_commit=0c6a7b3bf728b95c8b7b95328f94335e2bb2c967
pkgver=20220119.0c6a7b3
pkgrel=3
pkgdesc="Firmware files for Linux (kernels <5.3)"
url="https://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=summary"
license=('GPL2' 'GPL3' 'custom')
arch=('any')
depends=('linux<5.3')
makedepends=('git')
options=(!strip)
source=("git+https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git#commit=${_commit}?signed")
sha256sums=('SKIP')
validpgpkeys=('4CDE8575E547BF835FE15807A31B6BD72486CFD6') # Josh Boyer <jwboyer@fedoraproject.org>

_backports=(
)


_pick() {
  local p="$1" f d; shift
  for f; do
    d="$srcdir/$p/${f#$pkgdir/}"
    mkdir -p "$(dirname "$d")"
    mv "$f" "$d"
    rmdir -p --ignore-fail-on-non-empty "$(dirname "$f")"
  done
}

prepare() {
  cd ${_pkgbase}

  local _c
  for _c in "${_backports[@]}"; do
    git log --oneline -1 "${_c}"
    git cherry-pick -n "${_c}"
  done
}

pkgver() {
  cd ${_pkgbase}

  # Commit date + short rev
  echo $(TZ=UTC git show -s --pretty=%cd --date=format-local:%Y%m%d HEAD).$(git rev-parse --short HEAD)
}

build() {
  mkdir -p kernel/x86/microcode
  cat ${_pkgbase}/amd-ucode/microcode_amd*.bin > kernel/x86/microcode/AuthenticAMD.bin

  # Reproducibility: set the timestamp on the bin file
  if [[ -n ${SOURCE_DATE_EPOCH} ]]; then 
    touch -d @${SOURCE_DATE_EPOCH} kernel/x86/microcode/AuthenticAMD.bin
  fi

  # Reproducibility: strip the inode and device numbers from the cpio archive
  echo kernel/x86/microcode/AuthenticAMD.bin |
    bsdtar --uid 0 --gid 0 -cnf - -T - |
    bsdtar --null -cf - --format=newc @- > amd-ucode.img
}

package_linux4x-firmware-whence() {
  _pkgname=linux-firmware-whence
  provides=('linux-firmware-whence')
  conflicts=('linux-firmware-whence')

  cd "$_pkgbase"
  install -Dt "${pkgdir}/usr/share/licenses/${_pkgname}" -m644 WHENCE
}

package_linux4x-firmware() {
  provides=("${_pkgbase}")
  conflicts=("${_pkgbase}")
  depends=('linux4x-firmware-whence')
  
  cd ${_pkgbase}

  make DESTDIR="${pkgdir}" FIRMWAREDIR=/usr/lib/firmware install

  # Trigger a microcode reload for configurations not using early updates
  echo 'w /sys/devices/system/cpu/microcode/reload - - - - 1' |
    install -Dm644 /dev/stdin "${pkgdir}/usr/lib/tmpfiles.d/${_pkgbase}.conf"

  install -Dt "${pkgdir}/usr/share/licenses/${_pkgbase}" -m644 LICEN*

  # split
  cd "$pkgdir"
  _pick linux-firmware-nfp usr/lib/firmware/netronome
  _pick linux-firmware-nfp usr/share/licenses/${_pkgbase}/LICENCE.Netronome
   
  _pick linux-firmware-mellanox usr/lib/firmware/mellanox
  
  _pick linux-firmware-marvell usr/lib/firmware/{libertas,mwl8k,mwlwifi,mrvl}
  _pick linux-firmware-marvell usr/share/licenses/${_pkgbase}/LICENCE.{Marvell,NXP}
  
  _pick linux-firmware-qcom usr/lib/firmware/{qcom,a300_*}
  _pick linux-firmware-qcom usr/share/licenses/${_pkgbase}/LICENSE.qcom
  
  _pick linux-firmware-liquidio usr/lib/firmware/liquidio
  _pick linux-firmware-liquidio usr/share/licenses/${_pkgbase}/LICENCE.cavium_liquidio
  
  _pick linux-firmware-qlogic usr/lib/firmware/{qlogic,qed,ql2???_*,c{b,t,t2}fw-*}
  _pick linux-firmware-qlogic usr/share/licenses/${_pkgbase}/LICENCE.{qla1280,qla2xxx}
  
  _pick linux-firmware-bnx2x usr/lib/firmware/bnx2x*
}

package_linux4x-amd-ucode() {
  _pkgname=amd-ucode
  pkgdesc="Microcode update image for AMD CPUs"
  license=(custom)
  provides=('amd-ucode')
  conflicts=('amd-ucode')

  install -Dt "${pkgdir}/boot" -m644 amd-ucode.img

  install -Dt "${pkgdir}/usr/share/licenses/${_pkgname}" -m644 ${_pkgbase}/LICENSE.amd-ucode
}

package_linux4x-firmware-nfp() {
  _pkgname=linux-firmware-nfp
  pkgdesc+=" - nfp / Firmware for Netronome Flow Processors"
  depends=('linux4x-firmware-whence')
  provides=('linux-firmware-nfp')
  conflicts=('linux-firmware-nfp')

  mv -v linux-firmware-nfp/* "${pkgdir}"
}

package_linux4x-firmware-mellanox() {
  _pkgname=linux-firmware-mellonox
  pkgdesc+=" - mellanox / Firmware for Mellanox Spectrum switches"
  depends=('linux4x-firmware-whence')
  provides=('linux-firmware-mellonox')
  conflicts=('linux-firmware-mellonox')

  mv -v linux-firmware-mellanox/* "${pkgdir}"
}

package_linux4x-firmware-marvell() {
  _pkgname=linux-firmware-marvell
  pkgdesc+=" - marvell / Firmware for Marvell devices"
  depends=('linux4x-firmware-whence')
  provides=('linux-firmware-marvell')
  conflicts=('linux-firmware-marvell')

  mv -v linux-firmware-marvell/* "${pkgdir}"
}

package_linux4x-firmware-qcom() {
  _pkgname=linux-firmware-qcom
  pkgdesc+=" - qcom / Firmware for Qualcomm SoCs"
  depends=('linux4x-firmware-whence')
  provides=('linux-firmware-qcom')
  conflicts=('linux-firmware-qcom')

  mv -v linux-firmware-qcom/* "${pkgdir}"
}

package_linux4x-firmware-liquidio() {
  _pkgname=linux-firmware-liquidio
  pkgdesc+=" - liquidio / Firmware for Cavium LiquidIO server adapters"
  depends=('linux4x-firmware-whence')
  provides=('linux-firmware-liquidio')
  conflicts=('linux-firmware-liquidio')

  mv -v linux-firmware-liquidio/* "${pkgdir}"
}

package_linux4x-firmware-qlogic() {
  _pkgname=linux-firmware-qlogic
  pkgdesc+=" - qlogic / Firmware for QLogic devices"
  depends=('linux4x-firmware-whence')
  provides=('linux-firmware-qlogic')
  conflicts=('linux-firmware-qlogic')

  mv -v linux-firmware-qlogic/* "${pkgdir}"
}

package_linux4x-firmware-bnx2x() {
  _pkgname=linux-firmware-bnx2x
  pkgdesc+=" - bnx2x / Firmware for Broadcom NetXtreme II 10Gb ethernet adapters"
  depends=('linux4x-firmware-whence')
  provides=('linux-firmware-bnx2x')
  conflicts=('linux-firmware-bnx2x')

  mv -v linux-firmware-bnx2x/* "${pkgdir}"
}

# vim:set sw=2 et:
